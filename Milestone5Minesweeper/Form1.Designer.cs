﻿using MinesweeperClasses;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Milestone5Minesweeper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        static public Board myBoard;
        public Button[,] btnGrid;
        public GUIAdapter mineAdapter;

        static int gameGoal = 0;
        static int gameProgress = 0;

        Image bombIcon;
        Image flagIcon;

        public void populateGrid(int boardSize)
        {
            myBoard = new Board(boardSize);
            mineAdapter = new MinesweeperClasses.GUIAdapter(myBoard, boardSize);
            btnGrid = new Button[myBoard.Size, myBoard.Size];
            int buttonSize = panel1.Width / myBoard.Size;
            panel1.Height = panel1.Width;
            myBoard.assignBombs();

            Size iconSize = new Size(buttonSize-15, buttonSize-15);
            bombIcon = resizeImage(Properties.Resources.bomb, iconSize);
            flagIcon = resizeImage(Properties.Resources.flag, iconSize);

            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    btnGrid[r, c] = new Button();

                    btnGrid[r, c].Width = buttonSize;
                    btnGrid[r, c].Height = buttonSize;
                    btnGrid[r, c].Tag = r.ToString() + "," + c.ToString();

                    btnGrid[r, c].MouseUp += Grid_Button_MouseUp;
                    panel1.Controls.Add(btnGrid[r, c]);
                    btnGrid[r, c].Location = new Point(buttonSize * r, buttonSize * c);
                }
            }
        }

        private void Grid_Button_MouseUp(object sender, MouseEventArgs e)
        {
            Button clickedButton = (Button)sender;

            string[] strArr = (sender as Button).Tag.ToString().Split(',');
            int r = int.Parse(strArr[0]);
            int c = int.Parse(strArr[1]);
            if (e.Button == MouseButtons.Left)
            {
                mineAdapter.visitSquare(r, c);
            }
            else if (e.Button == MouseButtons.Right)
            {
                btnGrid[r, c].Image = flagIcon;
            }

            updateButtonLabels();


            if (gameProgress == gameGoal - 1)
            {
                button1.Text = "You Win!!!";
            }
        }


        public void checkGrid()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {

                    if (myBoard.theGrid[r, c].hasBomb != true)
                    {
                        gameGoal++;
                    }
                }
            }
        }

        public void updateButtonLabels()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].isVisited)
                    {
                        if (myBoard.theGrid[r, c].hasBomb != true && myBoard.theGrid[r, c].nextCount >= 1)
                        {
                            btnGrid[r, c].Text = "" + myBoard.theGrid[r, c].nextCount;
                            gameProgress++;
                        }
                        if (myBoard.theGrid[r, c].hasBomb != true && myBoard.theGrid[r, c].nextCount < 1)
                        {
                            btnGrid[r, c].Text = "~";
                            gameProgress++;
                        }
                        if (myBoard.theGrid[r, c].hasBomb)
                        {
                            revealAll();
                            button1.Text = "Game Over!!!";
                            timer1.Stop();
                        }
                    }

                }
            }

        }

        public void revealAll()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].hasBomb != true && myBoard.theGrid[r, c].nextCount >= 1)
                    {
                        btnGrid[r, c].Text = "" + myBoard.theGrid[r, c].nextCount;
                    }
                    if (myBoard.theGrid[r, c].hasBomb != true && myBoard.theGrid[r, c].nextCount < 1)
                    {
                        btnGrid[r, c].Text = "~";
                    }
                    if (myBoard.theGrid[r, c].hasBomb)
                    {
                        //btnGrid[r, c].Text = "Bomb";
                        
                        btnGrid[r, c].Image = bombIcon;
                    }
                }
            }
        }

        private static Image resizeImage(Image image, Size newSize)
        {
            Image newImage = new Bitmap(newSize.Width, newSize.Height);

            using (Graphics GFX = Graphics.FromImage((Bitmap)newImage))
            {
                GFX.DrawImage(image, new Rectangle(Point.Empty, newSize));
            }
            return newImage;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(199, 35);
            this.button1.TabIndex = 3;
            this.button1.Text = "Find the Mines!";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(13, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(627, 634);
            this.panel1.TabIndex = 2;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(440, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Timer:";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(539, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "0";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 719);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private Timer timer1;
        private Label label1;
        private Label label2;
    }
}

