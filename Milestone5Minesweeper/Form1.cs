﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone5Minesweeper
{
    public partial class Form1 : Form
    {

        private int seconds;
        public Form1()
        {

            //InitializeComponent();
            //populateGrid();
            checkGrid();
        }

        public Form1(int start)
        {
            InitializeComponent();
            timer1.Start();
            populateGrid(start);
            checkGrid();
            
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            seconds++;
            label2.Text = seconds.ToString();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }
    }
}
