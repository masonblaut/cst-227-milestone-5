﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinesweeperClasses
{
    public class GUIAdapter
    {
        static private Board myBoard;
        static private int boardSize;
        public GUIAdapter(Board theBoard, int theBoardSize)
        {
            myBoard = theBoard;
            boardSize = theBoardSize;
        }

        public void visitSquare(int row, int col)
        {


            if (myBoard.theGrid[row, col].hasBomb)
            {
                myBoard.theGrid[row, col].visited();
            }
            else
            {
                noBombFlood(row, col);
            }
        }


        public void noBombFlood(int x, int y)
        {
            if (x < 0 || x > boardSize - 1 || y < 0 || y > boardSize - 1 ||
                myBoard.theGrid[x, y].isVisited == true)
            {
                return;
            }

            myBoard.theGrid[x, y].visited();

            if (myBoard.theGrid[x, y].nextCount == 0)
            {
                noBombFlood(x - 1, y);
                noBombFlood(x, y - 1);
                noBombFlood(x, y + 1);
                noBombFlood(x + 1, y);
            }

        }

    }
}
